#include "types.h"

int main(int argc, char *argv[])
{	
	char* gameMemName = malloc(strlen(argv[1]) + 9);
	strcpy(gameMemName, "gameMem_");
	strcat(gameMemName, argv[1]);


	int shmfd = shm_open(gameMemName, O_CREAT|O_RDWR,0660);

	//specify the size of the shared memory region
    if(ftruncate(shmfd,sizeof(GameMem)) < 0)
    {
       perror("Failure in ftruncate()");
       return 0;
    }

	GameMem* shm = mmap(0,sizeof(GameMem),PROT_READ|PROT_WRITE,MAP_SHARED,shmfd,0);

	if(munmap(shm, sizeof(GameMem)) < 0)
		return -1;
	if(shm_unlink(gameMemName) < 0)
		return -1;

	printf("Done\n");
	return 0;

}