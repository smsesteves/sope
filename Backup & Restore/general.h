#include "stdlib.h"
#include "stdio.h"
#include <string.h>
#include <dirent.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

#include <time.h>

typedef struct {

	char* name;
	char* backup_path;
	int check;
	long long lastbackuptime;

} filetype;

typedef struct
{
	long long timestamp;
	filetype* ficheiros;
	int num_ficheiros;
} backup;

/**
 *
 * FUNÇÕES
 *
 */

// Devolve string concatenada
char *path_cat (const char *str1, char *str2) {
	size_t str1_len = strlen(str1);
	size_t str2_len = strlen(str2);
	char *result;

	result = malloc((str1_len+str2_len+2)*sizeof *result);
	strcpy (result,str1);
	int i,j;

	result[str1_len]='/';

	for(i=str1_len+1, j=0; ((i<(str1_len+str2_len+1)) && (j<str2_len));i++, j++) {
		result[i]=str2[j];
	}
	result[str1_len+str2_len+1]='\0';

	return result;
}

// Copia ficheiro
int copy_file(char * name,char *destino, char* ficheiro) {
	char *file_dst=path_cat(destino,name);
	char *file_ori=ficheiro;

	FILE *fp1,*fp2;
	char ch;
	fp1 =  fopen(file_ori,"r");
	fp2 =  fopen(file_dst,"w");

	while(1)
	{
		ch = fgetc(fp1);

		if(ch==EOF)
			break;
		else
			putc(ch,fp2);
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}
