#define _XOPEN_SOURCE      
#define _GNU_SOURCE

#include "general.h"

char* getname(char* buffer1)
{
    char * buffer = (char*)malloc(sizeof(char)*strlen(buffer1));
    strcpy(buffer, buffer1);
    int i=0;
    for(i=0;i<strlen(buffer);i++)
    {
        
        if(buffer[i]=='/')
        {
            buffer[i-1]='\0';
            return buffer;
        }
    }
    return buffer;
}

long long gettime(char* buffer)
{
    char * res;
    int i;

    for(i=strlen(buffer)-1; i>0; i--)
    {
        if(buffer[i]==' ')
        {
            res = malloc(sizeof(char)*(strlen(buffer)-1-i));
            memcpy(res, &buffer[i+1], strlen(buffer)-2-i);
            break;
        }
    }
    return atoll(res);
}

char* getpath(char* buffer) {
    
    char * res;
    int i = 0, j;
    for(i=0; i<strlen(buffer); i++) 
    {
        
        if(buffer[i] == '/')
            break;
    }

    for(j=strlen(buffer)-1; j>0; j--)
    {
        if(buffer[j]==' ') {
            res = malloc(sizeof(char)*(j-i+1));
            memcpy(res, &buffer[i], j-i);
            res[j-i] = '\0';
            break;
        }
    }
    return res;
}



int main(int argc, char *argv[])
{
    char* buffer= malloc(sizeof(char)*1024);
    struct tm * timeinfo = (struct tm *)malloc(sizeof(struct tm));

    // Verificar entradas
    if(argc!=3)
    {
        printf("Error: no directory\nShould be like: rstr origin_path destination_path\n\n");
        free(buffer);
        free(timeinfo);
        return -1;
    }

    int num_backups = 0;
    backup* backups = (backup*)malloc(sizeof(backup));

    // Abrir pasta
    const char *origin_path = argv[1];  // Directório de Origem
    struct dirent *dp; // Dir Pointer
    DIR *origin_dir = opendir(origin_path);

    // Deteção de Erros
    if(origin_dir == NULL)
    {
        printf("Diretorio nao existe\n");
        free(backups);
        free(buffer);
        free(timeinfo);
        return -1;
    }

    while ((dp = readdir(origin_dir)) != NULL)
    {
        if(!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") )
            continue;

        char *tmp = path_cat(origin_path, dp->d_name);
        struct stat sb;
        stat(tmp, &sb);

        // Se for uma PASTA
        if((sb.st_mode & S_IFMT) == S_IFDIR) 
        {
            
            // Procurar ficheiro __backupinfo__
            FILE* backupinfo = fopen(path_cat(tmp, "__backupinfo__"), "r");

            if(backupinfo == NULL) {
                continue;
            }

            // Espaço para nova info do Backup

            backups = (backup*)realloc(backups, sizeof(backup)*(num_backups+1));
            backups[num_backups].ficheiros = malloc (sizeof(filetype));

            // Buscar timestamp
            strptime(dp->d_name, "%y_%m_%d_%H_%M_%S", timeinfo);
            (*timeinfo).tm_isdst = 0;
            backups[num_backups].timestamp = (long long)mktime(timeinfo);

            int num_ficheiros = 0;
            
            while ( fgets(buffer, 1023, backupinfo) )
            {
                backups[num_backups].ficheiros = realloc ( backups[num_backups].ficheiros,sizeof(filetype)*(num_ficheiros+1));

                // NOME
                backups[num_backups].ficheiros[num_ficheiros].name = malloc(sizeof(char)*strlen(getname(buffer))+1);
                strcpy(backups[num_backups].ficheiros[num_ficheiros].name, getname(buffer));

                // TIMESTAMP
                backups[num_backups].ficheiros[num_ficheiros].lastbackuptime = gettime(buffer);

                // PATH
                backups[num_backups].ficheiros[num_ficheiros].backup_path = malloc(sizeof(char)*strlen(getpath(buffer))+1);
                strcpy(backups[num_backups].ficheiros[num_ficheiros].backup_path, getpath(buffer));

                num_ficheiros++;
            }

            backups[num_backups].num_ficheiros=num_ficheiros;
            num_backups++;

            fclose(backupinfo);
            free(tmp); tmp = NULL; //?
        
        }
    }

    
    closedir(origin_dir);
    
    //sorted alpha  
    int k=0;
    int l=0;    
    for (k = (num_backups - 1); k > 0; k--)
    {
        for (l = 1; l <= k; l++)
        {
            if (backups[l-1].timestamp > backups[l].timestamp)
            {
                backup* temp = malloc(sizeof(backup));
                *temp = backups[l-1];
                backups[l-1] = backups[l];
                backups[l] = *temp;
                free(temp);
            }
        }
    }
    
    timeinfo = localtime((const time_t *)&backups[0].timestamp);
    strftime(buffer, 150, "%y/%m/%d %H:%M:%S", timeinfo);
    printf("Version 1: %s\n", buffer);
    printf("-------------------------\n");
    int i=0;
    for(i=0;i<backups[0].num_ficheiros;i++)
    {
        printf("File %d: %s \n",i+1,backups[0].ficheiros[i].name);
    }
    printf("\n");
    
    int j;
    for(j=1;j<num_backups;j++)
    {
        timeinfo = localtime((const time_t *)&backups[j].timestamp);
        strftime(buffer, 150, "%y/%m/%d %H:%M:%S", timeinfo);
        printf("Version %d: %s\n",j+1,buffer);
        printf("-------------------------\n");
        int i=0; //atual
        int z=0; //anterior
        int encontrouadd=0;
        for(i=0;i<backups[j].num_ficheiros;i++)
        {
            for(z=0;z<backups[j-1].num_ficheiros;z++)
            {
                if(strcmp(backups[j].ficheiros[i].name,backups[j-1].ficheiros[z].name)==0)
                {
                    if(backups[j].ficheiros[i].lastbackuptime!=backups[j-1].ficheiros[z].lastbackuptime)
                    {
                        printf("File %d: %s || MODIFIED\n",i+1,backups[j].ficheiros[i].name);
                    }
                    encontrouadd=1;
                    break;
                }
            }
            if(encontrouadd==0)
            {
                timeinfo = localtime((const time_t *)&backups[j].ficheiros[i].lastbackuptime);
                strftime(buffer, 150, "%y/%m/%d %H:%M:%S", timeinfo);
                printf("File %d: %s || ADDED\n",i+1,backups[j].ficheiros[i].name);              
            }
            encontrouadd=0;
        }
        
        int encontroudel=0;
        for(i=0;i<backups[j-1].num_ficheiros;i++)
        {
            for(z=0;z<backups[j].num_ficheiros;z++)
            {
                if(strcmp(backups[j].ficheiros[z].name,backups[j-1].ficheiros[i].name)==0)
                {
                    encontroudel=1;
                    break;
                }
            }
            if(encontroudel==0)
            {
                printf("File %d: %s || DELETED\n",i+1,backups[j-1].ficheiros[i].name);
            }
            encontroudel=0;
        }
        printf("\n");
    }

    int num_versao=-1;

    do {    
        printf("Which version do you want to restore? \n");
        scanf("%s",buffer);
        num_versao = atoi(buffer);
    } while(num_versao <= 0 || num_versao > (num_backups));
    
    num_versao--;
    char * folder_destino=argv[2];


    struct stat sb={0};

    //Supondo que o diretorio existe

    if (stat(folder_destino, &sb) == -1) {
        if(mkdir(folder_destino, 0700)==-1 && EEXIST!=errno)
            return 0;
    }


     for(i=0;i<backups[num_versao].num_ficheiros;i++)
    {
        int forkReturn = fork();
        if(forkReturn == 0) {
            copy_file(backups[num_versao].ficheiros[i].name,folder_destino,path_cat(backups[num_versao].ficheiros[i].backup_path,backups[num_versao].ficheiros[i].name));
            exit(0);
        } 
    } 
    while (waitpid(-1, NULL, 0)!=-1) {
		printf("Waiting for child processes \n");
	}
		
	
    printf("VERSION %d RESTORED\n",num_versao+1);

    free(backups);
    free(buffer);

    return 1;
}
