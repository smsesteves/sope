#include "general.h"

void sigusr1_handler(int signo)
{
	printf("Received SIGUSR1 SIGNAL! Exit \n");
	
	    while (waitpid(-1, NULL, 0)!=-1) {
		printf("Waiting for child processes \n");
	}
	exit(0);
}

// Criar pasta para o Backup
char * create_folder(char* path) {
	time_t t;
	time (&t);
	struct tm * timeinfo = localtime(&t);
	(*timeinfo).tm_isdst = 0;
	char * timeString = malloc(150);
	strftime(timeString,150, "%y_%m_%d_%H_%M_%S", timeinfo);

	char * backupFolder = path_cat(path, timeString);

	//printf("%s", backupFolder);

	struct stat sb={0};

	//Supondo que o diretorio existe

	if (stat(path, &sb) == -1) {
		if(mkdir(path, 0700)==-1 && EEXIST!=errno)
			return NULL;
	}

	if (stat(backupFolder, &sb) == -1) {
		if(mkdir(backupFolder, 0777)==-1)
			return NULL;
	}

	return backupFolder;
}

// Escrever informação de cada Backup
void writebackupinfo(filetype* ficheiros,char* folder,int num_ficheiros) {
	int i;
	FILE* fx=fopen(path_cat(folder,"__backupinfo__"),"w+");

	for(i=0;i<num_ficheiros;i++)
		fprintf(fx,"%s %s %lld\n",ficheiros[i].name,ficheiros[i].backup_path,ficheiros[i].lastbackuptime);

	fclose(fx);
}


int main(int argc, char *argv[])
{
	
	
	if(signal(SIGUSR1,sigusr1_handler) <0)
	{
		fprintf(stderr,"Unable to install handler");
	}
	// Desactivar Buffers de STD
	setbuf(stdout, NULL);
	
	// Verificar entradas
	if(argc<=3)
	{
		printf("Error: no directory (required 2) and a gap time \n");
		return -1;
	}
	printf("TO EXIT: kill -SIGUSR1 %d\n",getpid());
	// Inicialização das variáveis
	const char *origin_path = argv[1];	// Directório de Origem
	long inc = (long) atoi(argv[3]); //Incremento do Timer
	filetype* ficheiros = (filetype*) malloc(sizeof(filetype));
	int num_ficheiros = 0, n_operacoes = 0;
	char * folder_destino;
	char * timeTest = malloc(150);

	// 
	while(1)
	{

		n_operacoes++;
		printf("Version %d\n",n_operacoes);
		int newFolderCreated = 0;
				
		// Abrir pasta
		struct dirent *dp; // Dir Pointer
		DIR *origin_dir = opendir(origin_path);

		// Deteção de Erros
		if(origin_dir == NULL)
		{
			printf("Directory not exist\n");
			return -1;
		}
		
		// Percorrer fiif Backup
		while ((dp = readdir(origin_dir)) != NULL)
		{
		
			char *tmp = path_cat(origin_path, dp->d_name);
			struct stat sb;
			stat(tmp, &sb);
			// Se for um REGULAR FILE
			if((sb.st_mode & S_IFMT) == S_IFREG)	{

				// if FULL BACKUP
				if(n_operacoes==1)
				{					
					// Verificar se é preciso criar pasta
					if(newFolderCreated==0) {
						// Criação da pasta unica de Backups
						folder_destino = create_folder(argv[2]);

						if(folder_destino==NULL)
						{
							printf("Error\n");
							return -1;
						}

						newFolderCreated = 1;
					}

					num_ficheiros++;
					ficheiros = realloc(ficheiros,sizeof(filetype)*num_ficheiros);
					// Nome
					ficheiros[num_ficheiros-1].name = malloc(strlen(dp->d_name)+1);
					strcpy(ficheiros[num_ficheiros-1].name,dp->d_name);
					// Check
					ficheiros[num_ficheiros-1].check = 1;
					// Backup Path
					ficheiros[num_ficheiros-1].backup_path = malloc(strlen(folder_destino)+1);
					strcpy(ficheiros[num_ficheiros-1].backup_path,folder_destino);
					// Time
					time_t t;
					time (&t);
					struct tm * timeinfo = localtime(&t);
					(*timeinfo).tm_isdst = 0;
					ficheiros[num_ficheiros-1].lastbackuptime = (long long)mktime(timeinfo);
					printf("File %s ADDED\n",ficheiros[num_ficheiros-1].name);
					// Copiar Ficheiro
					int forkReturn = fork();
					if(forkReturn == 0) {
						copy_file(dp->d_name,folder_destino,tmp);
						exit(0);
					}
				
				// if NOT FULL BACKUP
				} else {
				
					int i, encontrou=0;
					// Percorre estrutura de ficheiros[]
					for(i = 0; i < num_ficheiros; i++)
					{
						if(strcmp(ficheiros[i].name,dp->d_name) == 0)
						{
							encontrou=1;
							ficheiros[i].check=1;
							
						
 
							//ANDRE CORRIGIR
							if((long long)sb.st_mtime+3600 > (long long)ficheiros[i].lastbackuptime)
							{
						
								// Verificar se é preciso criar pasta
								if(newFolderCreated==0) {
									// Criação da pasta unica de Backups
									folder_destino = create_folder(argv[2]);
									if(folder_destino==NULL)
									{
										printf("Error\n");
										return -1;
									}

									newFolderCreated = 1;
								}
								printf("File %s Saved\n",ficheiros[i].name);
								// Copiar Ficheiro
								int forkReturn = fork();
								if(forkReturn == 0) {
									copy_file(dp->d_name,folder_destino,tmp);
									exit(0);
								}
								time_t t;
								time (&t);
								struct tm * timeinfo = localtime(&t);
								(*timeinfo).tm_isdst = 0;
								ficheiros[i].lastbackuptime=mktime(timeinfo);
								strcpy(ficheiros[i].backup_path,folder_destino);
							}
						}
					}

					// Verificar se é novo
					if(encontrou==0)
					{
						// Verificar se é preciso criar pasta
						if(newFolderCreated==0) {
							// Criação da pasta unica de Backups
							
							folder_destino = create_folder(argv[2]);
							if(folder_destino==NULL)
							{
								printf("Error\n");
								return -1;
							}

							newFolderCreated = 1;
						}

						num_ficheiros++;
						ficheiros=realloc(ficheiros,sizeof(filetype)*num_ficheiros);
						ficheiros[num_ficheiros-1].name=malloc(strlen(dp->d_name)+1);
						strcpy(ficheiros[num_ficheiros-1].name,dp->d_name);
						ficheiros[num_ficheiros-1].check=1;
						ficheiros[num_ficheiros-1].backup_path=malloc(strlen(folder_destino)+1);
						strcpy(ficheiros[num_ficheiros-1].backup_path, folder_destino);
						time_t t;
						time (&t);
						struct tm * timeinfo = localtime(&t);
						(*timeinfo).tm_isdst = 0;
						ficheiros[num_ficheiros-1].lastbackuptime=mktime(timeinfo);
						printf("File %s ADDED\n",ficheiros[num_ficheiros-1].name);
						int forkReturn = fork();
						if(forkReturn == 0) {
							copy_file(dp->d_name,folder_destino,tmp);
							exit(0);
						}
						
					}			
				}
			}
			free(tmp); tmp=NULL;
		}

		// Verificar e eliminar ficheiros

		if(n_operacoes>1)
		{
			int i;
			for(i=0;i<num_ficheiros;i++)
			{
				// Verificar se é para eliminar ficheiro
				if(ficheiros[i].check == 0) {

					// Verificar se é preciso criar pasta
					if(newFolderCreated==0) {
						// Criação da pasta unica de Backups
						
						folder_destino = create_folder(argv[2]);
						if(folder_destino==NULL)
						{
							printf("Error\n");
							return -1;
						}

						newFolderCreated = 1;
					}
					
					printf("File %s Deleted\n",ficheiros[i].name);
					
					// Eliminar Ficheiro
					int j;
					for(j = i; j + 1 < num_ficheiros; j++) 
					{			
						filetype* tmpfile = malloc(sizeof(filetype));
						*tmpfile = ficheiros[j];
						ficheiros[j] = ficheiros[j+1];
						ficheiros[j+1] = *tmpfile;
						free(tmpfile);
					}

					num_ficheiros--;
					i--;
					ficheiros = realloc(ficheiros, sizeof(filetype)*num_ficheiros);
				}
			}
		}
		 
		if(newFolderCreated==1)
			writebackupinfo(ficheiros,folder_destino,num_ficheiros);

		int i;
		for(i = 0; i < num_ficheiros; i++)
			ficheiros[i].check = 0;
 
		
		free(folder_destino); folder_destino=NULL;
		closedir(origin_dir);
		
		sleep(inc);
	}
	free(timeTest);
	
	free(ficheiros);
	
	return 0;
}
